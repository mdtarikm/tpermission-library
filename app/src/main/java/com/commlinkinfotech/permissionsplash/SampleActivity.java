package com.commlinkinfotech.permissionsplash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.citl.tpermissionlibrary.TPermissionUtility;

public class SampleActivity extends AppCompatActivity implements TPermissionUtility.PermissionResultListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        new TPermissionUtility(this).setOnPermissionResultListener(this).checkIfPermissionsAreGranted();

    }

    @Override
    public void onPermissionGranted() {
        Snackbar.make(getWindow().getDecorView(), "Permission Granted.", Snackbar.LENGTH_LONG).show();
        // Do your codes here...
    }

    @Override
    public void onPermissionDenied() {
        new TPermissionUtility(this).setOnPermissionResultListener(this).requestPermissionsWithRationale();
    }

    @Override
    public void onPermissionDeniedPermanently() {
        new TPermissionUtility(this).setOnPermissionResultListener(this).takeToSettingsToAllowPermissionsManually();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        new TPermissionUtility(this).setOnPermissionResultListener(this).onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TPermissionUtility.REQUEST_MULTIPLE_PERMISSION_CODE) {
            new TPermissionUtility(this).setOnPermissionResultListener(this).checkIfPermissionsAreGranted();
        }
    }
}
