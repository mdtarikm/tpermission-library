package com.citl.tpermissionlibrary;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tarik on 13-Mar-18.
 */

public class TPermissionUtility
{

    public static final int REQUEST_MULTIPLE_PERMISSION_CODE = 1001;
    public static final String HAS_PERMISSION_REQUESTED = "permission_requested";

    private Activity activity;
    private PermissionResultListener permissionResultListener;
    private String rationaleTitle = "Permission Required";
    private String rationaleText = "Please allow all requested permissions to continue using the app.";
    private String rationaleTextForSettings = "Please allow all requested permissions to continue using the app.";

    private static String[] listOfRequiredPermissions;
    private Set<String> listOfAlreadyRequestedPermissions = new HashSet<String>();

    public TPermissionUtility(Activity activity) {
        this.activity = activity;
        this.listOfRequiredPermissions = retrievePermissionsFromManifest();
        this.listOfAlreadyRequestedPermissions = PreferenceManager.getDefaultSharedPreferences(this.activity.getApplicationContext()).getStringSet(HAS_PERMISSION_REQUESTED, new HashSet<String>());
    }


    public TPermissionUtility setOnPermissionResultListener(PermissionResultListener permissionResultListener) {
        this.permissionResultListener = permissionResultListener;
        return this;
    }


    /**
     *
     * @param title Title to show when explaining the user about the required permissions.
     * @return Instance of this class.
     */
    public TPermissionUtility setRationaleTitle(String title) {
        this.rationaleTitle = title;
        return this;
    }

    /**
     *
     * @param text Explanation to show the user about the required permissions.
     * @return Instance of this class.
     */
    public TPermissionUtility setRationaleText(String text) {
        this.rationaleText = text;
        return this;
    }

    /**
     *
     * @param text Text to show when we take the use to system settings to approve
     *             the permissions.
     * @return Instance of this class.
     */
    public TPermissionUtility setRationaleTextForSettings(String text) {
        this.rationaleTextForSettings = text;
        return this;
    }

    /**
     * The method to check the availability of required permissions.
     * It also requests for any required permissions if its not granted yet.
     * @return Instance of this class.
     */

    public TPermissionUtility checkIfPermissionsAreGranted() {

        boolean hasAllPermission = true;

        for (String permission : listOfRequiredPermissions) {
            hasAllPermission
                    = hasAllPermission && (ActivityCompat.checkSelfPermission(this.activity, permission) == PackageManager.PERMISSION_GRANTED);
        }

        if (hasAllPermission) {
            /*
            * We have all required permissions: and we can
            * 1. Add name of requested permissions to SharedPreferences
            * 2. Move to next step.
            * */
            if (listOfRequiredPermissions.length < listOfAlreadyRequestedPermissions.size()) {
                PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext())
                        .edit()
                        .putStringSet(HAS_PERMISSION_REQUESTED, new HashSet<String>(Arrays.asList(listOfRequiredPermissions))).commit();
            }
            if (this.permissionResultListener != null)
                this.permissionResultListener.onPermissionGranted();
        } else {
           /*
            * We don't have all required permissions. One or more permission is missing.
            * */

            boolean shouldShowMessage = true;

            for (String permission : listOfRequiredPermissions) {
                shouldShowMessage
                        = shouldShowMessage && (ActivityCompat.shouldShowRequestPermissionRationale(this.activity, permission));
            }

            if (shouldShowMessage) {

                /*
                * We should show an explanation and request for permissions.
                * */
                this.requestPermissionsWithRationale();

            } else {
                /*
                * No need to show any explanation.
                *
                * Check if there is any permission/s which was not requested earlier.
                * If YES, request permission for that/those.
                * If NO, then probably user denied with "NEVER_ASK_AGAIN".
                * */

                if (getUnrequestedPermissionsList().size() > 0) {
                    this.requestPermissionsWithRationale();

                } else {
                    /*
                    * The user should be taken to the Settings to give the permissions explicitly.
                    * */
                    if (this.permissionResultListener != null)
                        this.permissionResultListener.onPermissionDeniedPermanently();
                }

            }

        }

        return this;
    }


    /**
     *
     * @return Array of permissions those were listed in Manifest using uses-permission tag.
     */
    public String[] retrievePermissionsFromManifest() {
        try {
            if (listOfRequiredPermissions == null) {
                return this.activity
                        .getPackageManager()
                        .getPackageInfo(activity.getPackageName(), PackageManager.GET_PERMISSIONS)
                        .requestedPermissions;
            } else {
                return listOfRequiredPermissions;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return new String[]{};
        }
    }

    /**
     *
     * @return Returns the set of permissions those are required,
     * but is not requested yet.
     */
    private HashSet<String> getUnrequestedPermissionsList() {
        HashSet<String> setOfRequiredUnrequestedPermissions = new HashSet<String>(Arrays.asList(listOfRequiredPermissions));
        setOfRequiredUnrequestedPermissions.removeAll(listOfAlreadyRequestedPermissions);
        return setOfRequiredUnrequestedPermissions;
    }

    /**
     * Request for permission with an explanation.
     */
    public void requestPermissionsWithRationale() {

        /*
        * Show a convincing message to user and explain why we need these
        * permissions. Possibly with a dialog.
        * */

        new AlertDialog.Builder(this.activity)
                .setTitle(this.rationaleTitle)
                .setMessage(this.rationaleText)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActivityCompat.requestPermissions(TPermissionUtility.this.activity, listOfRequiredPermissions, REQUEST_MULTIPLE_PERMISSION_CODE);

                        /*
                        * Add name of requested permissions to SharedPreferences
                        * */
                        PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext())
                                .edit()
                                .putStringSet(HAS_PERMISSION_REQUESTED, new HashSet<String>(Arrays.asList(listOfRequiredPermissions))).commit();
                        dialog.dismiss();
                    }
                }).create().show();
    }


    /**
     * Method to process the requestResults.
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_MULTIPLE_PERMISSION_CODE) {

            /*
            * Determine if any of the requested permissions were granted.
            * */
            boolean granted = true;

            /*
            * Determine if any of the permission was denied with "Never ask again"
            * */
            boolean shouldShowRationale = false;

            for (int i = 0; i < permissions.length; i++) {
                if (granted = granted && (grantResults[i] == PackageManager.PERMISSION_GRANTED)) {

                } else {
                    shouldShowRationale = shouldShowRationale || (ActivityCompat.shouldShowRequestPermissionRationale(this.activity, permissions[i]));
                }
            }

            /*
            * Now if granted == false, that means one/more permission was denied.
            * If shouldShowRationale = false also, that means, one/more permission was denied with "Never Ask Again"
            * */

            if (granted) {
                /*
                * All permissions are granted.
                * */

                if (this.permissionResultListener != null)
                    this.permissionResultListener.onPermissionGranted();

            } else {

                if (shouldShowRationale) {
                    /*
                    * Handle normal deny, possibly we can request for permission again.
                    * */

                    if (this.permissionResultListener != null)
                        this.permissionResultListener.onPermissionDenied();

                } else {
                    /*
                    * Handle never ask again. Possibly we can try to take the user to app-specific Settings page.
                    * */

                    if (this.permissionResultListener != null)
                        this.permissionResultListener.onPermissionDeniedPermanently();

                }
            }

        }


    }

    public void takeToSettingsToAllowPermissionsManually() {

        /*
        * Show a convincing message to user and explain why we need these
        * permissions. Then take him to settings. If you use this method,
        * you should check the result in onActivityResult() by calling the
        * checkIfPermissionsAreGranted() again.
        * In this case, resultCode will be RESULT_CANCEL (=0) always. We used
        * startActivityForResult just as a trick to catch the event of coming back
        * from settings-screen.
        * */

        new AlertDialog.Builder(this.activity)
                .setTitle(this.rationaleTitle)
                .setMessage(this.rationaleTextForSettings)
                .setCancelable(false)
                .setPositiveButton("GO TO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", TPermissionUtility.this.activity.getPackageName(), null);
                        intent.setData(uri);
                        TPermissionUtility.this.activity.startActivityForResult(intent, REQUEST_MULTIPLE_PERMISSION_CODE);
                        dialog.dismiss();

                    }
                }).create().show();
    }

    /*
    * Listener to handle permission grant/deny events.
    * */
    public interface PermissionResultListener {

        void onPermissionGranted();

        void onPermissionDenied();

        void onPermissionDeniedPermanently();

    }

}
